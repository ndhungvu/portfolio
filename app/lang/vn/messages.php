<?php
/*Language Vietnamese*/
return array(
	'created_success' 			=> 'Dữ liệu đã được tạo mới thành công.',
	'created_error'				=> 'Dữ liệu tạo không thành công.',
	'updated_success'			=> "Dữ liệu đã được cập nhật thành công.",
	'updated_error'				=> 'Dữ liệu cập nhật không thành công.',
	'deleted_success'			=> 'Dữ liệu đã được xóa thành công.',
	'deleted_error'				=> 'Dữ liệu xóa không thành công',
	'change_status_success'		=> 'Trạng thái thay đổi thành công',
	'change_status_error'		=> 'Trạng thái thay đổi không thành công',
);
?>