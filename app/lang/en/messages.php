<?php
/*Language English*/
return array(
	'created_success' 			=> 'The data has been created successfully.',
	'created_error'				=> 'The data is not successfully created',
	'updated_success'			=> "The data has been updated successfully.",
	'updated_error'				=> 'The data is not successfully updated',
	'deleted_success'			=> 'The data has been deleted successfully.',
	'deleted_error'				=> 'The data is not successfully deleted',
	'change_status_success'		=> 'Status has changed successfully',
	'change_status_error'		=> 'Status is not successfully changed',
);
?>