<?php

use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class CategoryNew extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories_news';	

	public function category() {
		return $this->belongsTo('Category','category_id' ,'id');
	}

	public static function getCategoriesByNewID($newID, $type) {
		return \CategoryNew::where('new_id',$newID)->where('type', $type)->get();
	}

	public static function deleteCategoriesByNewID($newID, $type) {
		return \CategoryNew::where('new_id',$newID)->where('type', $type)->delete();
	}
}
