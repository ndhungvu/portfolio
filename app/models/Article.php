<?php
use Cviebrock\EloquentTypecast\EloquentTypecastTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class Article extends Eloquent {
    protected $table = "articles";
    const ACTIVE = 1;
    const UNACTIVE = 0;

    /*This is function used get articles by category id*/
    public static function getArticlesByCategoryID($category_id, $limit = -1, $offset = 0) {
    	return \Article::join('categories_news','articles.id','=','categories_news.new_id')
    		->join('categories','categories_news.category_id','=','categories.id')
    		->where('categories.status', Category::ACTIVE)->where('categories.id',$category_id)
    		->where('categories_news.type','A')->where('articles.status',Article::ACTIVE)
    		->orderBy('articles.created_at', 'DESC')
    		->select('articles.*')
    		->limit($limit)->offset($offset)->get();
    }

    /*This is function used get article by alias*/
    public static function getArticleByAlias($alias) {
        return Article::where('alias', $alias)->where('status', Article::ACTIVE)->first();
    }

    /*This is function used get article by alias_en*/
    public static function getArticleByAliasEn($alias_en) {
        return Article::where('alias_en', $alias_en)->where('status', Article::ACTIVE)->first();
    }
}