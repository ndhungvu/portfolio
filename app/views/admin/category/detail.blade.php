@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.categories')}}">Categories</a></li>
            <li class="active">Detail</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.category.edit', $category->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#vi" aria-controls="vi" role="tab" data-toggle="tab">Vietnamese</a></li>
                                    <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">English</a></li>                                    
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="vi">
                                        <table id="category_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$category->id}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$category->title}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$category->alias}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$category->description}}</td>
                                                </tr>
                                                <!--<tr>
                                                    <td>Position</td>
                                                    <td>{{$category->position}}</td>
                                                </tr>-->
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($category->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>800, 'height'=> 120))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$category->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Position</td>
                                                    <td>{{$category->position}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$category->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$category->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="en">
                                        <table id="category_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$category->id}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$category->title_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$category->alias_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$category->description_en}}</td>
                                                </tr>
                                                <!--<tr>
                                                    <td>Position</td>
                                                    <td>{{$category->position}}</td>
                                                </tr>-->
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($category->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>800, 'height'=> 120))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$category->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Position</td>
                                                    <td>{{$category->position}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$category->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$category->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                                
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop