@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <p><h3 class="box-title">Users</h3></p>
                    <a class="btn btn-primary btn-sm" style="margin-right: 3px;" href="{{URL::route('admin.user.add')}}" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-fw fa-plus"></i></a>
                    @if (count($users) > 0)
                    <a class="btn btn-danger btn-sm jsDeleteAll" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Delete All"><i class="fa fa-fw fa-trash"></i></a>
                    @endif
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                            @if (!empty($users))
                                {{Form::open(array('url'=>'admin/user/deleteAll', 'id'=>'frmDeleteAll', 'class'=>'form-horizontal'))}}
                                <table id="users" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="jsCheckboxAll"/></th>
                                            <th>#</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Name</th>
                                            <th>Avatar</th>
                                            <th>By Group</th>
                                            <th>Status</th>
                                            <th>Created date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $index => $user)
                                        <tr role="row" class="odd">
                                            <td><input type="checkbox" name="id[]" value="{{$user->id}}" class="jsCheckbox"/></td>
                                            <td class="sorting_1">{{$index + 1}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->last_name}} {{$user->first_name}}</td>
                                            <td>{{ HTML::image($user->avatar, $alt="Avatar", $attributes = array('class'=>'img-rounded','width'=>80, 'height'=> 80))}}</td>
                                            <td>{{$user->group->name}}</td>
                                            <td>{{$user->status == 1 ? 'Active' : 'UnActive'}}</td>
                                            <td>{{$user->created_at}}</td>
                                            <td>
	                                            <a class="btn btn-success btn-xs" href="{{URL::route('admin.user.detail', $user->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
	                                            <a class="btn btn-primary btn-xs" href="{{URL::route('admin.user.edit', $user->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
	                                            <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.user.delete', $user->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{Form::close()}}
                                @if (count($users) == 0)<h3 class="box-title"><small>No result</small></h3> @endif
                                {{$users->links()}}
                            @endif
                           </div>
                       </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop