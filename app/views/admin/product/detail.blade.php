@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{URL::route('admin.products')}}">products</a></li>
            <li class="active">Detail</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="col-xs-6">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-primary btn-sm" style="float: right;" href="{{URL::route('admin.product.edit', $product->id)}}">Edit</a>
                    </div>
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#vi" aria-controls="vi" role="tab" data-toggle="tab">Vietnamese</a></li>
                                    <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">English</a></li>                                    
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="vi">
                                        <table id="product_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$product->id}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$product->title}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$product->alias}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$product->description}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Content</td>
                                                    <td>{{$product->content}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($product->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Slide</td>
                                                    <td>
                                                        @if(!empty($product_images))
                                                            @foreach($product_images as $key => $value)
                                                            {{ HTML::image($value->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120, 'style'=>'margin-right: 20px;'))}}
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Categories</td>
                                                    <td>
                                                        <?php
                                                            $arrCats = array();
                                                        ?>
                                                        @if(!empty($categories))
                                                            @foreach($categories as $key => $value)
                                                            <?php array_push($arrCats, $value->category->id);
                                                                if(!in_array($value->category->parent_id, $arrCats)) {
                                                            ?>
                                                            - {{$value->category->title}}<br>
                                                            <?php }else{?>
                                                            <span style="margin-left: 20px;">+ {{$value->category->title}}</span><br>
                                                            <?php }
                                                            ?>
                                                            
                                                            
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Hits</td>
                                                    <td>{{$product->hits}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Highlight</td>
                                                    <td>{{$product->is_highlight == 1 ? 'Yes' : 'No'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$product->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$product->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$product->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="en">
                                        <table id="product_detail" class="table table-bordered" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-3">ID</td>
                                                    <td>{{$product->id}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Title</td>
                                                    <td>{{$product->title_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alias</td>
                                                    <td>{{$product->alias_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>{{$product->description_en}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Content</td>
                                                    <td>{{$product->content_en}}</td>
                                                </tr>                                        
                                                <tr>
                                                    <td>Image</td>
                                                    <td>{{ HTML::image($product->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120))}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Slide</td>
                                                    <td>
                                                        @if(!empty($product_images))
                                                            @foreach($product_images as $key => $value)
                                                            {{ HTML::image($value->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>120, 'height'=> 120, 'style'=>'margin-right: 20px;'))}}
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Categories</td>
                                                    <td>
                                                        <?php
                                                            $arrCats = array();
                                                        ?>
                                                        @if(!empty($categories))
                                                            @foreach($categories as $key => $value)
                                                            <?php array_push($arrCats, $value->category->id);
                                                                if(!in_array($value->category->parent_id, $arrCats)) {
                                                            ?>
                                                            - {{$value->category->title}}<br>
                                                            <?php }else{?>
                                                            <span style="margin-left: 20px;">+ {{$value->category->title}}</span><br>
                                                            <?php }
                                                            ?>
                                                            
                                                            
                                                            @endforeach
                                                        @endif
                                                    </td>                                      
                                                </tr>
                                                <tr>
                                                    <td>Hits</td>
                                                    <td>{{$product->hits}}</td>
                                                </tr>                                       
                                                <tr>
                                                    <td>Highlight</td>
                                                    <td>{{$product->is_highlight == 1 ? 'Yes' : 'No'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{{$product->status == 1 ? 'Active' : 'UnActive'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created date</td>
                                                    <td>{{$product->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Created update</td>
                                                    <td>{{$product->updated_at}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </div>                                
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop