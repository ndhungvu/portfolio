@extends('admin/layouts.dashboard')

@section('content')
<section class="content-header">
    <span>
        <ol class="breadcrumb" style="margin-bottom: 0px;">
            <li><a href="{{URL::route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Articles</li>
        </ol>
    </span>
</section>
<section class="content">
    <div class="row">
         <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <p><h3 class="box-title">Articles</h3></p>
                    <a class="btn btn-primary btn-sm" style="margin-right: 3px;" href="{{URL::route('admin.article.add')}}" data-toggle="tooltip" data-placement="top" title="Add"><i class="fa fa-fw fa-plus"></i></a>
                    @if (count($articles) > 0)
                    <a class="btn btn-danger btn-sm jsDeleteAll" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Delete All"><i class="fa fa-fw fa-trash"></i></a>
                    @endif
                </div>
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                            @if (!empty($articles))
                                {{Form::open(array('url'=>'admin/article/deleteAll', 'id'=>'frmDeleteAll', 'class'=>'form-horizontal'))}}
                                <table id="articles" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="jsCheckboxAll"/></th>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Highlight</th>                                          
                                            <th>Status</th>
                                            <th>Created date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($articles as $index => $article)
                                        <tr role="row" class="odd">
                                            <td><input type="checkbox" name="id[]" value="{{$article->id}}" class="jsCheckbox"/></td>
                                            <td class="sorting_1">{{$index + 1}}</td>
                                            <td>{{$article->title}}</td>
                                            <td>{{ HTML::image($article->image, $alt="Image", $attributes = array('class'=>'img-rounded','width'=>80, 'height'=> 80))}}</td>
                                            <td>
                                                @if($article->is_highlight)
                                                    <span class="btn btn-info btn-xs" role="button"><i class="fa fa-fw fa-star"></i></span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($article->status == 1)
                                                <a class="btn btn-info btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.article.changeStatus', $article->id)}}" attr-status = "{{$article->status}}" data-toggle="tooltip" data-placement="top" title="UnActive" role="button"><i class="fa fa-fw fa-unlock"></i></a>
                                                @else
                                                <a class="btn btn-warning btn-xs jsChangeStatus" href= "javascript:;" attr-href="{{URL::route('admin.article.changeStatus', $article->id)}}" attr-status = "{{$article->status}}" data-toggle="tooltip" data-placement="top" title="Active" role="button"><i class="fa fa-fw fa-unlock-alt"></i></a>
                                                @endif
                                            </td>
                                            <td>{{$article->created_at}}</td>
                                            <td>
	                                            <a class="btn btn-success btn-xs" href="{{URL::route('admin.article.detail', $article->id)}}" data-toggle="tooltip" data-placement="top" title="View" role="button"><i class="fa fa-fw fa-eye"></i></a>
	                                            <a class="btn btn-primary btn-xs" href="{{URL::route('admin.article.edit', $article->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" role="button"><i class="fa fa-fw fa-edit"></i></a>
	                                            <a href="javascript:;" class="btn btn-danger btn-xs jsDelete" attr_href="{{URL::route('admin.article.delete', $article->id)}}" data-toggle="tooltip" data-placement="top" title="Delete" role="button"><i class="fa fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{Form::close()}}
                                @if (count($articles) == 0)<h3 class="box-title"><small>No result</small></h3> @endif
                                {{$articles->links()}}
                            @endif
                           </div>
                       </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop