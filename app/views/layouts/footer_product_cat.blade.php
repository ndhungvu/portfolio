<div class="header">
    <!--menu-top mobile-->
    <div class="bg-menu-sp show-sp" id="bg-menu-sp"><a href="#" id="click-menu-top" class=""><img src="images/icon-menu-sp.png" alt=""></a></div>   
    <!--end menu-top mobile-->        
    <div class="inner inner-sp">            
            <a href="#"><img src="images/top-logo.jpg" alt="" class="logo"></a>
            <div class="top-right">
                <div class="share-search">
                     <form class="form-lan">
                        <select class="select-lan"   id="demo-htmlselect" readonly>
                            <option data-imagesrc="http://192.168.0.106/vieteufood/images/img-vietnam.jpg" value="vietnam">Việt Nam</option>
                            <option value="thailan">Thai Lan</option>
                            <option value="trungquoc">Trung Quoc</option>
                        </select>
                    </form>
                    <form class="form-search">
                        <input type="text" class="input-form-search" id="" value="Tìm Kiếm"
                        onfocus="(this.value == 'Tìm Kiếm') && (this.value = '')"
                        onblur="(this.value == '') && (this.value = 'Tìm Kiếm')"> 
                        <input type="submit" class="submit-form-search" value="" readonly>
                    </form>
                   
                    <div class="share-sp">
                    <a href="#"><img src="images/top-facebook.jpg" alt=""></a>
                    <a href="#"><img src="images/top-p.jpg" alt=""></a>
                    <a href="#"><img src="images/top-google.jpg" alt=""></a>
                    <a href="#"><img src="images/top-twice.jpg" alt=""></a>
                    <a href="#"><img src="images/top-youtube.jpg" alt=""></a>
                    
                    </div>
                </div>
                <div class="menu-top" id="">
                    <ul>
                        <li><a href="#"  id="trangchu">trang chủ</a></li>
                        <li><a href="#">giới thiệu</a></li>
                        <li><a href="#">cơ sở sản xuất</a></li>
                        <li><a href="#">sản phẩm</a></li>
                        <li><a href="#">góc ẩm thực</a></li>
                        <li><a href="#">tin tức sự kiện</a></li>
                        <li><a href="#">dấu ấn</a></li>
                        <li class="li-last"><a href="#">liên hệ</a></li> 
                    </ul>
                </div>
               
            </div>
    </div>
</div>
<!--end header-->
<div class="slide-page">
    <img src="images/sanpham/banner-page.png" alt="" class="banner-page">
</div>  
<p class="p-title-sp show-sp"><a href="#abc">Trang chủ</a> > Sản phẩm</p>