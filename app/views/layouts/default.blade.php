
<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- meta tag, meta name -->
    <meta name="description" content="" />
    <title>Portfolio | Vu Nguyen</title>
    <!--==== Favicons============== --> 

    {{HTML::style('assets/default/themes/style.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/style.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/fonts/stylesheet.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/slider_scroll.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/bootstrap.min.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/animate.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/layout.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/shop.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/jquery.fancybox.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/responsive.css?ver=1')}}
    {{HTML::style('assets/default/themes/css/artist-woocommerce.css?ver=1')}}
    {{HTML::style('assets/default/css/woocommerce-layout.css?ver=1')}}
    <link rel='stylesheet' href='/assets/default/css/woocommerce-smallscreen.css?ver=2.3.13' type='text/css' media='only screen and (max-width: 768px)' />
    {{HTML::style('assets/default/css/woocommerce.css?ver=1')}}
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Unkempt%3A400%7CLove+Ya+Like+A+Sister%3A400&#038;subset=latin&#038;ver=1437382226' type='text/css' media='all' />

    <!--JS-->
    {{HTML::script('/assets/default/themes/js/jquery/jquery.js?ver=1.11.1')}} 
    {{HTML::script('/assets/default/themes/js/jquery/jquery-migrate.min.js?ver=1.2.1')}}  
    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key&#038;sensor=false&#038;ver=4.0.7'></script>

    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <style type="text/css" title="dynamic-css" class="options-output">body{font-family:Unkempt;font-weight:400;font-style:normal;color:#000000;}h1,h2,h3,h4,h5,h6{font-family:"Love Ya Like A Sister";font-weight:400;font-style:normal;color:#000000;}</style><!-- wp head -->
    <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-35935151-8', 'auto');
    ga('send', 'pageview');

</script>
</head>
<body class="home page page-id-706 page-template page-template-includesection-home-php">
    <!-- Preloader Start
    <div id="preloader">
        <div id="status">Loading </div>
    </div>
    Preloader End -->
    <div id="hs_top_wrap"> </div>
    <!--======Container-Start======-->
    <div class="container">
        <!-- Header-->
        @include('layouts.header')
        <!--Banner-->
        @include('layouts.banner')
        <!--Content-->
        {{$content}}
        <div class="clearboth"></div>
        <!--Footer-->
        @include('layouts.footer')
        <div id="hs_bottom_wrap"></div>

        <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {"ajax_url":"\/wp\/artist\/wp-admin\/admin-ajax.php","i18n_view_cart":"View Cart","cart_url":"http:\/\/kamleshyadav.com\/wp\/artist\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
        /* ]]> */
        </script>
        {{HTML::script('/assets/default/js/frontend/add-to-cart.min.js?ver=2.3.13')}}
        {{HTML::script('/assets/default/js/jquery-blockui/jquery.blockUI.min.js?ver=2.60')}}

        <script type='text/javascript'>
        /* <![CDATA[ */
        var woocommerce_params = {"ajax_url":"\/wp\/artist\/wp-admin\/admin-ajax.php"};
        /* ]]> */
        </script>
        {{HTML::script('/assets/default/js/frontend/woocommerce.min.js?ver=2.3.13')}}
        {{HTML::script('/assets/default/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1')}}
        <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {"ajax_url":"\/wp\/artist\/wp-admin\/admin-ajax.php","fragment_name":"wc_fragments"};
        /* ]]> */
        </script>
        {{HTML::script('/assets/default/themes/js/cart-fragments.min.js?ver=2.3.13')}}
        {{HTML::script('/assets/default/themes/js/validation.js')}}
        {{HTML::script('/assets/default/themes/js/custum.js')}}
        {{HTML::script('/assets/default/themes/js/bootstrap.js')}}
        {{HTML::script('/assets/default/themes/js/jquery.mixitup.js')}}
        {{HTML::script('/assets/default/themes/js/bxslider.js')}}
        {{HTML::script('/assets/default/themes/js/jquery.fancybox.js')}}
        {{HTML::script('/assets/default/themes/js/jquery.easing.js')}}
        {{HTML::script('/assets/default/themes/js/smoothscroll.js')}}
        {{HTML::script('/assets/default/themes/js/wow.js')}}
        {{HTML::script('/assets/default/themes/js/ie.js')}}
        {{HTML::script('/assets/default/themes/js/comment-reply.min.js?ver=4.0.7.js')}}
    </body>
</html>
