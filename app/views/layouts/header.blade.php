<div class="row-fluid">
    <div class="span12">
        <div class="span3 logo">
            <a href="/">
                <img src="/uploads/image/index.png" alt="logo"/>
            </a>
        </div>
        <!-- Desktop-Main-Menu-Start -->
        <div class="hs_menu_toggle navbar-toggle collapsed" data-toggle="collapse" data-target="#artist_main_menu">Menu</div>
        <div class="span9 hs_menu artist_home">
            <div class="menu-myartist-container">
                <ul id="artist_main_menu" class="hs_main_menu_bar collapse navbar-collapse pull-right">
                    <li id="menu-item-1429" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-706 current_page_item menu-item-1429">
                        <a href="/">Home</a>
                    </li>
                    <li id="menu-item-1430" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1430">
                        <a href="/sample-page-2.html">Pages</a>
                        <ul class="sub-menu">
                            <li id="menu-item-1439" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1439"><a href="/about-us.html">About</a>
                            </li>
                            <li id="menu-item-1438" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1438"><a href="/blog.html">Blog Full Width</a>
                            </li>
                            <li id="menu-item-1436" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1436"><a href="/blog-with-left-sidebar.html">Blog With Left Sidebar</a>
                            </li>
                            <li id="menu-item-1435" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1435"><a href="/blog-with-right-sidebar.html">Blog With Right Sidebar</a>
                            </li>
                            <li id="menu-item-1409" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1409"><a href="/pencil-sketch.html">Blog Single Page</a>
                            </li>
                            <li id="menu-item-1434" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1434"><a href="/search.html">Search</a>
                            </li>
                        </ul>
                    </li>
                    <li id="menu-item-1431" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1431">
                        <a href="/services.html">Services</a>
                    </li>
                    <li id="menu-item-1437" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1437">
                        <a href="/blog.html">Just Blogging</a>
                    </li>
                    <li id="menu-item-1428" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1428">
                        <a href="/shop.html">Shop</a>
                    </li>
                    <li id="menu-item-1432" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1432">
                        <a href="/portfolio.html">Portfolio</a>
                    </li>
                    <li id="menu-item-1433" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1433">
                        <a href="/contact.html">Contact</a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>
</div>