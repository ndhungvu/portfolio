<div class="row">
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="span12">
            <!-- Creative-title-Start -->
                <div class="row-fluid hs_overflow_hidden">
                    <div class="span12 hs_creative wow fadeInDown">
                        <div class="span1 hs_creative_thumb wow bounceIn">
                            <p class="hs_font_icon_30">7</p>
                        </div>
                        <div class="span11">
                            <div class="span11">
                                <h1> Innovation, Perfection and Creativity at its best.</h1>
                            </div>
                            <div class="span11 hs_left hs_creative_info">Artist Theme is WP Version of Responsive and Innovative HTML5 Template which is a NOMINEE for BEST CSS AWARD. A great combination of Unique Sketch Design and elements of HTML5, styled with CSS 3, a Powerful Backend which Gives user, a ability to menu-item-object-customize and Change almost Everything.
                            </div>
                        </div>
                    </div>
                </div><!-- Creative-title-End -->
            </div>
        </div>
        <div class="clearboth"></div>
            <div class="clearboth">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="span12 hs_top_sixty wow fadeInLeft">
                                        <h1 class="lined"> Our Services </h1>
                                    </div>
                                    <div class="span12 hs_left hs_top_sixty hs_overflow_hidden">
                                        <div class="span3 hs_post wow bounceInDown">
                                            <div class="span12 hs_service_thumb text-center">
                                                <p class="hs_font_icon hs_icon_padding_top_85">9</p>
                                            </div>
                                            <div class="span12 hs_services_title text-center">
                                                <a href="/services/company-branding/"> Company Branding </a>
                                            </div>
                                        </div>
                                        <div class="span3 hs_post wow bounceInDown">
                                            <div class="span12 hs_service_thumb text-center">
                                                <p class="hs_font_icon hs_icon_padding_top_85">b</p>
                                            </div>
                                            <div class="span12 hs_services_title text-center">
                                                <a href="/services/software-developer/"> Software Developer </a>
                                            </div>
                                        </div>
                                        <div class="span3 hs_post wow bounceInDown">
                                            <div class="span12 hs_service_thumb text-center">
                                                <p class="hs_font_icon hs_icon_padding_top_85">></p>
                                            </div>
                                            <div class="span12 hs_services_title text-center">
                                                <a href="/services/logo-designer/"> Logo Designer </a>
                                            </div>
                                        </div>
                                        <div class="span3 hs_post wow bounceInDown">
                                            <div class="span12 hs_service_thumb text-center">
                                                <p class="hs_font_icon hs_icon_padding_top_85">Z</p>
                                            </div>
                                            <div class="span12 hs_services_title text-center">
                                                <a href="/services/web-developer/"> Web Developer </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearboth"></div>
                <div class="clearboth">
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- See-Our-Latest-Works Start -->
                            <div class="row-fluid">
                                <div class="span12 hs_overflow_hidden">
                                    <div class="span12 hs_left hs_top_fifty wow fadeInLeft">
                                        <h1 class="lined">See Our Latest Works</h1>
                                    </div>
                                    <div class="span4 hs_main_latest wow fadeInDown">
                                        <a href="/portfolio/web-development/">
                                            <div class="span12 hs_port_thumb">
                                                <div class="span12 hs_latest"> <img src="/uploads/image/2-300x300.jpg" alt="Portfolio Thumb"/></div>
                                            </div>
                                        </a>
                                        <div class="span12 text-center"><a href="/portfolio/web-development/">Sweet Baby</a> </div>
                                    </div>
                                    <div class="span4 hs_main_latest wow fadeInDown">
                                        <a href="/portfolio/artist-graphic-item/">
                                            <div class="span12 hs_port_thumb">
                                                <div class="span12 hs_latest"> <img src="/uploads/image/12-300x300.jpg" alt="Portfolio Thumb"/></div>
                                            </div>
                                        </a>
                                        <div class="span12 text-center"><a href="/portfolio/artist-graphic-item/">Guitar Girl</a> </div>
                                    </div>
                                    <div class="span4 hs_main_latest wow fadeInDown">
                                        <a href="/portfolio/artist-html-item/">
                                            <div class="span12 hs_port_thumb">
                                                <div class="span12 hs_latest"> <img src="/uploads/image/5-300x300.jpg" alt="Portfolio Thumb"/></div>
                                            </div>
                                        </a>
                                        <div class="span12 text-center"><a href="/portfolio/artist-html-item/">The Mask Of Art</a> </div>
                                    </div>
                                    <div class="span6 hs_see_more offset5"> <a href="/?post_type=portfolio" class="hs_more_btn"> See More </a> </div>
                                </div>
                            </div><!-- See-Our-Latest-Works End -->
                        </div>
                    </div>
                    <div class="clearboth"></div>
                </div>
                <div class="row-fluid"></div>
            </div>
        </div>
    </div>
    <div class="clearboth">
        <div class="row-fluid">
            <div class="span6">
                <!-- Blog-Start -->
                <div class="hs_overflow_hidden">
                    <div class="span12 hs_bottom_fourty hs_left wow fadeInLeft">
                        <h1 class="lined">Just Blogging </h1>
                    </div>
                    <div class="span12 hs_post_blog hs_left wow fadeInDown">
                        <a href="/say-something-new/">
                            <div class="span4 hs_blog_thumb">
                                <div class="span12 hs_bloging"><img width="183" height="183" src="/uploads/image/blog12-183x183.jpg" class="attachment-artist-blowidget-size wp-post-image" alt="blog1" /> </div>
                            </div>
                        </a>
                        <div class="span8">
                            <h2> <a href="/say-something-new/">Say something new </a> </h2>
                        </div>
                        <div class="span8 hs_meta_tag">
                            <h3> <a href="/say-something-new/">13 September 2014 </a> </h3>
                        </div>
                        <div class="span8"> Good words are worth much, and cost little. Friendship requires great... </div>
                    </div>
                <div class="span12 hs_post_blog hs_left wow fadeInDown">
                <a href="/micro-niche-wordpress-theme/">
                    <div class="span4 hs_blog_thumb">
                        <div class="span12 hs_bloging"><img width="183" height="183" src="/uploads/image/1-183x183.jpg" class="attachment-artist-blowidget-size wp-post-image" alt="1" /> </div>
                    </div>
                </a>
                <div class="span8">
                    <h2> <a href="/micro-niche-wordpress-theme/">Keep Calm And Be Prepared </a> </h2>
                </div>
                <div class="span8 hs_meta_tag">
                    <h3> <a href="/micro-niche-wordpress-theme/">13 September 2014 </a> </h3>
                </div>
                <div class="span8">You have to rely on your preparation. You got to really be passionate and try to prepare more than anyone else, and put yourself in a position to succeed, and... </div>
                </div>
            </div>
        <!-- Blog-End -->
        </div>
        <div class="span6">
            <!-- Client-say-Start -->
            <div class="span12 hs_bottom_fourty hs_left wow fadeInLeft hs_delay_02">
                <h1 class="lined">What Our Client Say</h1>
            </div>
            <div class="span12 hs_testimonial hs_left">
                <!-- Slider start -->
                <div class="span12 hs_slider_testo">
                    <ul class="bxslider">
                        <li>
                            <div class="span12 ">
                            <div class="span2 offset1 hs_comma_first"></div>
                            <div class="span9 hs_testimonial_info offset1">
                            I have been enormously thankful for Kamleshyadav technical support in the design and creation of my website. He has been a skilful and patient teacher and adviser.</div>
                            <div class="span8 hs_testimonial_title offset1">
                            <img width="70" height="70" src="/uploads/image/user1.jpg" alt="" class="img-circle pull-left" style="margin-right:15px;" />
                            <h3>Stephanie Dale </h3>
                            <p></p>
                            </div>
                            <div class="span2 hs_comma_second"> </div>
                            </div>
                        </li>
                        <li>
                            <div class="span12 ">
                            <div class="span2 offset1 hs_comma_first"></div>
                            <div class="span9 hs_testimonial_info offset1">
                            Kamleshyadav was genuinely interested in my business and what I needed to convey with my website and social media connections. It also helps that he’s quick.</div>
                            <div class="span8 hs_testimonial_title offset1">
                            <img width="70" height="70" src="/uploads/image/user2.jpg" alt="" class="img-circle pull-left" style="margin-right:15px;" />
                            <h3>Jill Moonie</h3>
                            <p></p>
                            </div>
                            <div class="span2 hs_comma_second"> </div>
                            </div>
                        </li>
                        <li>
                            <div class="span12 ">
                                <div class="span2 offset1 hs_comma_first"></div>
                                <div class="span9 hs_testimonial_info offset1">
                                Kamleshyadav helped Northern Rivers Wildlife Carers update our website with a new theme and some nifty features including a link to an online giving site.We would recommend him to anyone .</div>
                                <div class="span8 hs_testimonial_title offset1">
                                    <img width="70" height="70" src="/uploads/image/user3.jpg" alt="" class="img-circle pull-left" style="margin-right:15px;" />
                                    <h3>Sandy Norris</h3>
                                    <p></p>
                                </div>
                                <div class="span2 hs_comma_second"> </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- Slider End -->
            </div>
        <!-- Client-say-End -->
        </div>
    </div>
    <div class="clearboth"></div>
</div>
<div class="clearboth"></div>
<div class="clearboth">
    <div class="row-fluid">
        <div class="span12">
        <!-- Our-Clients-Start -->
            <div class="row-fluid">
                <div class="span12">
                    <div class="span12 wow fadeInLeft">
                        <h1 class="lined"> Our Clients </h1>
                    </div>
                    <div class="span12 hs_left hs_slider_base">
                     <!-- Slider Start -->
                        <div class="span12">
                            <div class="hs_slider_client" style="width:100%; float:left;">
                                <ul class="bxslider_partner">
                                    <li>
                                        <div class="hs_client_logo wow bounceInRight">
                                            <a href="#"> 
                                                <img src="/uploads/image/activeden-light1.png" alt="themeforest"/>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hs_client_logo wow bounceInRight">
                                            <a href="#">
                                                <img src="/uploads/image/graphicriver-light2.png" alt="themeforest"/>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hs_client_logo wow bounceInRight">
                                            <a href="#"> 
                                                <img src="/uploads/image/activeden-light1.png" alt="themeforest"/>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hs_client_logo wow bounceInRight">
                                            <a href="#">
                                                <img src="/uploads/image/graphicriver-light2.png" alt="themeforest"/>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                             </div>
                        </div>
                    <!-- Slider End -->
                    </div>
                </div>
            </div>
        <!-- Our-Clients-Start -->
        </div>
    </div>
    <div class="clearboth"></div>
</div>