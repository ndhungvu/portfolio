<div class="menu-left show-pc">
    <div class="title-menu-left">
        {{Lang::get('site.products');}}
    </div>
    <ul class="ul-menu-left">
        @if ($lang == 'vn')
            <li class="@if(empty($data['category'])) active @endif"><a href="/san-pham-moi">{{Lang::get('site.new_products');}}</a></li>
            @foreach($data['categories'] as $key => $cat)
            <li class="@if(!empty($data['category']) && $cat->id == $data['category']->id) active @endif">
                <a href="/san-pham/{{$cat->alias}}">{{$cat->title}}</a>
            </li>
            @endforeach
        @else
            <li class="@if(empty($data['category'])) active @endif"><a href="/products?lang=en">{{Lang::get('site.new_products');}}</a></li>
            @foreach($data['categories'] as $key => $cat)
            <li class="@if(!empty($data['category']) && $cat->id == $data['category']->id) active @endif">
                <a href="/products/{{$cat->alias_en}}?lang=en">{{$cat->title_en}}</a>
            </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="content-right">
    <div class="slide-sanpham">
        <img src="{{asset('/').$data['product_images'][0]->image}}" alt="" class="img-lar jsLargeImage" width="376" height="300">
        <ul class="list-img-small">
            @foreach($data['product_images'] as $key => $product_image)
            <li><img src="{{asset('/').$product_image->image}}" alt=""  class="img-small jsSmallImage"></li>
            @endforeach            
        </ul>
    </div>
    @if ($lang == 'vn')
        <div class="c-slide-sanpham">
            <p class="t-c-slide-sanpha">{{$data['product']->title}}</p>
            <p class="c-c-slide-sanpha">{{$data['product']->description}}</p>
        </div>
        <div class="product-content">
            {{$data['product']->content}}
        </div>
    @else
        <div class="c-slide-sanpham">
            <p class="t-c-slide-sanpha">{{$data['product']->title_en}}</p>
            <p class="c-c-slide-sanpha">{{$data['product']->description_en}}</p>
        </div>
        <div class="product-content">
            {{$data['product']->content_en}}
        </div>
    @endif
</div>