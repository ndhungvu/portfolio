<div class="menu-left show-pc">
    @if ($lang == 'vn')
    <div class="title-menu-left">
        <a href="/lien-he">{{Lang::get('site.contact');}}</a>
    </div>
    <ul class="ul-menu-left">
        <li><a href="/gioi-thieu">{{Lang::get('site.introduce');}}</a></li>
        <li><a href="/co-so-san-xuat">{{Lang::get('site.manufacture');}}</a></li>
        <li><a href="/dau-an">{{Lang::get('site.mark');}}</a></li>
        <li><a href="/chinh-sach">{{Lang::get('site.policy');}}</a></li>        
    </ul>
    @else
    <div class="title-menu-left">
        <a href="/contact?lang=en">{{Lang::get('site.contact');}}</a>
    </div>
    <ul class="ul-menu-left">
        <li><a href="/introduce?lang=en">{{Lang::get('site.introduce');}}</a></li>
        <li><a href="/manufacture?lang=en">{{Lang::get('site.manufacture');}}</a></li>
        <li><a href="/mark?lang=en">{{Lang::get('site.mark');}}</a></li>
        <li><a href="/policy?lang=en">{{Lang::get('site.policy');}}</a></li>        
    </ul>
    @endif
</div>
<div class="content-right">
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
    var myCenter=new google.maps.LatLng(10.754355, 106.666913);
    function initialize() {
        var mapProp = {
            center:myCenter,
            zoom:17,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        var marker=new google.maps.Marker({
            position:myCenter,
        });

        marker.setMap(map);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <div id="googleMap" style="width:100%; height:300px;"></div>

</div>