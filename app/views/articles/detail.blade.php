<div class="menu-left show-pc">
    <div class="title-menu-left">
        @if ($lang == 'vn')
        <a href="/tin-tuc">{{Lang::get('site.news');}}</a>
        @else
        <a href="/articles?lang=en">{{Lang::get('site.news');}}</a>
        @endif
    </div>
</div>
<div class="content-right">
    <div class="gioithieu">
        <img src="{{asset('/').$data['article']->image}}" alt="" class="img-amthuc-xem">
        @if ($lang == 'vn')
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data['article']->title}}</p>
            <p class="gt-p-c c-ttam">{{$data['article']->description}}</p>
        </div>
        <div class="product-content">{{$data['article']->content}}</div>
        @else
        <div class="gt-c ttam">
            <p class="gt-p-t t-ttam">{{$data['article']->title_en}}</p>
            <p class="gt-p-c c-ttam">{{$data['article']->description_en}}</p>
        </div>
        <div class="product-content">{{$data['article']->content_en}}</div>
        @endif
    </div>
</div>