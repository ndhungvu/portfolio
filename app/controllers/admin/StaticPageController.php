<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response;
class StaticPageController extends \AdminController {

     public function getCreate() {
        $title = 'Static Pages - Create';
        return View::make('/admin/staticpage/create', compact('title', 'categories'));
    }

    public function postCreate() {
        $input = array(
           'title' => Input::get('title'),
           'content' => Input::get('content'),         
        );

        $valid = array(
           'title' => 'required',
           'content' => 'required',
           
        );

        $v = Validator::make($input, $valid);
        if($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v);
        }

        $static_page = new \StaticPage();
        $static_page->title = Input::get('title');
        $static_page->alias = \alias($static_page, $static_page->title);
        $static_page->description = Input::get('description');
        $static_page->content = Input::get('content');

        /*English*/
        $static_page->title_en = Input::get('title_en');        
        $static_page->description_en = Input::get('description_en');
        $static_page->content_en = Input::get('content_en');

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $static_page->image = 'uploads/image/'.$imageName.$ext;
            }
        }else {
            $static_page->image = Input::get('image_old');
        }

        if($static_page->save()) {
            return Redirect::route('admin.static_page.edit', $static_page->id)->with('flashSuccess', 'Static page is created');
        }else {
            return Redirect::back()->with('flashError', 'Static page created fail');
        }
    }  

    public function getEdit($id) {
    	$title = "Static Pages - Edit";
    	$static_page = \StaticPage::where('id', $id)->first();
    	if(!empty($static_page)) {    		
    		return View::make('/admin/staticpage/edit',compact('title', 'static_page'));
    	}else {
    		
    	}
    }

    public function postEdit($id) {
        $input = array(
           'title' => Input::get('title'),
           'content' => Input::get('content'),         
        );

        $valid = array(
           'title' => 'required',
           'content' => 'required',           
        );

        $v = Validator::make($input, $valid);
        if($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v);
        }

        $static_page = \StaticPage::where('id',$id)->first();
        $static_page->title = Input::get('title');
        $static_page->alias = \alias($static_page, $static_page->title);
        $static_page->description = Input::get('description');
        $static_page->content = Input::get('content');

        /*English*/
        $static_page->title_en = Input::get('title_en');        
        $static_page->description_en = Input::get('description_en');
        $static_page->content_en = Input::get('content_en');

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $static_page->image = 'uploads/image/'.$imageName.$ext;
            }
        }else {
            $static_page->image = Input::get('image_old');
        }

        if($static_page->save()) {
            return Redirect::route('admin.static_page.edit', $static_page->id)->with('flashSuccess', 'Static page is updated');
        }else {
            return Redirect::back()->with('flashError', 'Static page updated fail');
        }
    }

    /*Delete static page*/
    public function postDelete($id) {
        echo $id; exit;
        $static_page = \StaticPage::where('id',$id)->first();
        if(!empty($static_page)) {
            if($static_page->delete()) {                
               return Redirect::route('admin.static_page.add')->with('flashSuccess', 'Static page is deleted');
            }else {
                return Redirect::back()->with('flashError', 'Static page deleted fail');
            }
        }else {

        }
    } 
}