<?php
namespace Admin;
use View, Input, Validator, Redirect, Auth, Hash, Response;
class ArticleController extends \AdminController {

    public function index() {
    	$title = 'Articles';
    	$articles = \Article::orderBy('created_at', 'DESC')->paginate(10);
    	return View::make('/admin/article/index', compact('title', 'articles'));
    }

    public function getDetail($id) {
    	$title = 'Articles - Detail';
    	$article = \Article::where('id',$id)->first();
        /*Get categories of article*/
        $categories = \CategoryNew::getCategoriesByNewID($id, 'A');
    	if(!empty($article)) {
    		return View::make('/admin/article/detail',compact('title', 'article','categories'));
    	}
    }

    public function getCreate() {
    	$title = 'Articles - Create';
    	/*Get categories*/
    	$categories = \Category::where('parent_id', 0)->where('id','!=',1)->where('status', \Group::ACTIVE)->get();
    	return View::make('/admin/article/create', compact('title', 'categories'));
    }

    public function postCreate() {
    	$input = array(
    	   'title' => Input::get('title'),
    	   'content' => Input::get('content'),    	   
    	);

    	$valid = array(
    	   'title' => 'required',
    	   'content' => 'required',
    	   
    	);

    	$v = Validator::make($input, $valid);
    	if($v->fails()) {
    		return Redirect::back()->withInput()->withErrors($v);
    	}

        $article = new \Article();
        $article->title = Input::get('title');
        $article->alias = \alias($article, $article->title);        
        $article->description = Input::get('description');
        $article->content = Input::get('content');

        /*English*/
        $article->title_en = Input::get('title_en');
        $article->alias_en = !empty($article->title_en) ? \alias($article, $article->title_en) : $article->alias;
        $article->description_en = Input::get('description_en');
        $article->content_en = Input::get('content_en');

        $article->is_highlight = Input::get('is_highlight');
        $article->status = Input::get('status');

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $article->image = 'uploads/image/'.$imageName.$ext;
            }
        }else {
            $article->image = Input::get('image_old');
        }

    	if($article->save()) {            
            /*Save categories of article*/
            $categories = Input::get('category_id');
            if(!empty($categories)) {
                foreach ($categories as $key => $id) {
                    $category_new = new \CategoryNew();
                    $category_new->category_id = $id;
                    $category_new->new_id = $article->id;
                    $category_new->type = 'A';
                    if(!$category_new->save()) {
                        return Redirect::back()->with('flashError', 'Article category created fail');
                    }
                }
            }
    		return Redirect::route('admin.article.detail', $article->id)->with('flashSuccess', 'Article is created');
    	}else {
    		return Redirect::back()->with('flashError', 'Article created fail');
    	}
    }

    public function getEdit($id) {
    	$title = "Articles - Edit";
    	$article = \Article::where('id', $id)->first();
    	if(!empty($article)) {
    		/*Get categories*/
            $categories = \Category::where('parent_id', 0)->where('id','!=',1)->where('status', \Group::ACTIVE)->get();           
            /*Get cateories of article*/
            $article_categories = \CategoryNew::getCategoriesByNewID($id, 'A');
            $arrArticleCategory = array();
            foreach ($article_categories as $key => $cat) {
               $arrArticleCategory = array_add($arrArticleCategory, $key, $cat->category_id);
            }
    		return View::make('/admin/article/edit',compact('title', 'article', 'categories', 'arrArticleCategory'));
    	}else {
    		
    	}
    }

    public function postEdit($id) {
        $input = array(
           'title' => Input::get('title'),
           'content' => Input::get('content'),         
        );

        $valid = array(
           'title' => 'required',
           'content' => 'required',
           
        );

        $v = Validator::make($input, $valid);
        if($v->fails()) {
            return Redirect::back()->withInput()->withErrors($v);
        }

        $article = \Article::where('id',$id)->first();
        $article->title = Input::get('title');
        $article->alias = \alias($article, $article->title);        
        $article->description = Input::get('description');
        $article->content = Input::get('content');

        /*English*/
        $article->title_en = Input::get('title_en');
        $article->alias_en = !empty($article->title_en) ? \alias($article, $article->title_en) : $article->alias;
        $article->description_en = Input::get('description_en');
        $article->content_en = Input::get('content_en');

        $article->is_highlight = Input::get('is_highlight') == 'on' ? 1 : 0;
        $article->status = Input::get('status') == 'on' ? 1 : 0;

        $image_tmp = Input::get('image_tmp');
        if(!empty($image_tmp)) {
            $path_image_tmp = base_path().$image_tmp;
            
            $imageName = md5(time());
            $size = getimagesize(base_path().$image_tmp);
            $ext = image_type_to_extension($size[2]);

            $path_image_new = base_path().'/uploads/image/'.$imageName.$ext;
            $content = file_get_contents($path_image_tmp);            
            if(file_put_contents($path_image_new, $content)) {
                /*Delete image tmp*/ 
                \File::delete($path_image_tmp);
                $article->image = 'uploads/image/'.$imageName.$ext;
            }
        }else {
            $article->image = Input::get('image_old');
        }

        if($article->save()) {            
            /*Save categories of article*/
            $categories = Input::get('category_id');
            if(!empty($categories)) {
                /*Delete categories old of article*/
                \CategoryNew::deleteCategoriesByNewID($article->id, 'A');
                foreach ($categories as $key => $id) {
                    $category_new = new \CategoryNew();
                    $category_new->category_id = $id;
                    $category_new->new_id = $article->id;
                    $category_new->type = 'A';
                    if(!$category_new->save()) {
                        return Redirect::back()->with('flashError', 'Article category updated fail');
                    }
                }
            }
            return Redirect::route('admin.article.detail', $article->id)->with('flashSuccess', 'Article is updated');
        }else {
            return Redirect::back()->with('flashError', 'Article updated fail');
        }
    }

    /*Delete Article*/
    public function postDelete($id) {
    	$article = \Article::where('id', $id)->first();
    	if(!empty($article)) {
    		if($article->delete()) {
                /*Delete categories old of article*/
                \CategoryNew::deleteCategoriesByNewID($id, 'A');               
                return Response::json(array('error'=>false, 'message'=> 'Article is deleted'));
            }
            return Response::json(array('error'=>true, 'message'=> 'Article deleted fail'));
    	}else {

    	}
    }

    /*Delete all articles*/
    public function postDeleteAll() {
        $checkboxes = $_POST['id'];
        $ok = true;
        foreach ($checkboxes as $id) {
            $article = \Article::where('id', $id)->first();
            if(!$article->delete()) {
                $ok = false;
                break;
            }
            /*Delete categories old of article*/
            \CategoryNew::deleteCategoriesByNewID($id, 'A');
        }

        if($ok) {
            return Response::json(array('error'=>false, 'message'=> 'Articles is deleted'));
        }else {
           return Response::json(array('error'=>true, 'message'=> 'Articles delete fail'));
        }
    }

     /*This is function used change status of article*/
    public function postChangeStatus($id) {
        $article = \Article::where('id',$id)->first();
        if(!empty($article)) {
            $article->status = $article->status == 0 ? 1 : 0;
            if($article->save()) {
                return Response::json(array('error'=>false, 'message'=> \Lang::get('messages.change_status_success'), 'data'=>$article));
            }else {
                return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
            }
        }else {
            return Response::json(array('error'=>true, 'message'=> \Lang::get('messages.change_status_error')));
        }
    }
}