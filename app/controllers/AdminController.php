<?php

class AdminController extends BaseController {
    protected $layout = 'admin.layouts.dashboard';
    public $thumbs = array(
        'A' => '120X120',
        'B' => '400X150',
        'C' => '400X300',
        'D' => '1400X500'
    );
    public function __construct() {                   
        $static_pages = \StaticPage::getStaticPages();
        View::share('static_pages', $static_pages);
    }
	public function index() {
		$this->layout->title = "Dashboard";
        $this->layout->content = View::make('admin/user/login');
	}

	public function uploadImage() {     
        $file = Input::file('image');
        if (!empty($file)) {
            $fileSize = @$file->getSize();
            if ( ! $fileSize || ! $file->getMimeType()) {
            	return Response::json(array('status'=>false, 'message'=> 'Upload image is fail'));
            }
            $imageName = md5(time());
            $mimetype = preg_replace('/image\//', '', $file->getMimeType());
            $pathUpload = 'uploads/tmp/';
            Input::file('image')->move($pathUpload, $imageName.'.'.$mimetype);
            $strImage = '/'.$pathUpload.$imageName.'.'.$mimetype;
            return Response::json(array('status'=>true, 'message'=> 'Upload image is successful', 'data'=> $strImage));
        }
        return Response::json(array('status'=>false, 'message'=> 'Upload image is fail'));
    }

    public function resizeImage($image_root_path, $thumb_type = 'A', $path = 'uploads/image/') {
        $thumb = explode("X", $this->thumbs[$thumb_type]);
        $width = $thumb[0];
        $height = $thumb[1];
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        /*Get file name of image root*/
        $path_parts = pathinfo($image_root_path);        
        $file_name = $path_parts['filename'];
        $ext = $path_parts['extension'];
        $path = $path.$file_name.'-'.$width.'x'.$height.'.'.$ext;
        if(Image::make($image_root_path)->resize($width, $height)->save($path))
            return true;
        return false;
    }
}
