<?php

class CulinarController extends BaseController {
	protected $layout = 'layouts.product_cat';
	const LIMIT = 10;
	

	public function detail($alias) {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
			/*Get product*/
			$culinar = Article::getArticleByAliasEn($alias);			
		}else {
			$lang = 'vn';
			/*Get product*/
			$culinar = Article::getArticleByAlias($alias);		
		}		
		App::setLocale($lang);	
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/goc-am-thuc/'.$culinar->alias,
			'en' => '/culinar/'.$culinar->alias_en.'?lang=en'
		);

		//dump($category);
		if(empty($culinar)) {
			echo '404'; exit;
		}

		$data = array();
		$data['culinar'] = $culinar;

		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = Lang::get('site.culinar');
        $this->layout->content = View::make('/culinar/detail', compact('data'));
	}

	public function lists() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';			
		}else {
			$lang = 'vn';			
		}		
		App::setLocale($lang);	
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/goc-am-thuc',
			'en' => '/culinar?lang=en'
		);
	
		/*Get articles*/
		$culinars = Article::join('categories_news','articles.id','=','categories_news.new_id')
    			->where('categories_news.type','A')
    			->join('categories','categories_news.category_id','=','categories.id')
    			->where('categories.id',3)->where('categories.status',Category::ACTIVE)
                ->where('articles.status',Article::ACTIVE)
    			->orderBy('categories.created_at','DESC')->orderBy('articles.created_at','DESC')
    			->select(array('articles.*'))
    			->paginate($this::LIMIT);
	
		$data = array();
		$data['culinars'] = $culinars;
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = Lang::get('site.culinar');
        $this->layout->content = View::make('/culinar/lists', compact('data'));		
	}

}
