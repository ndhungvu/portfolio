<?php

class OnePageController extends BaseController {
	protected $layout = 'admin.layouts.dashboard';
	public function index() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}
		$lang_url = array(
			'vn' => '/',
			'en' => '?lang=en'
		);
		return View::make('onepage', compact('lang','lang_url'));
	}

}
