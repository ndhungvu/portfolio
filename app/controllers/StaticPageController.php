<?php
class StaticPageController extends BaseController {
	protected $layout = 'layouts.product_cat';
	public function introduce() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/gioi-thieu',
			'en' => '/introduce?lang=en'
		);
		$data = StaticPage::getByID(1);
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Introduce";
        $this->layout->content = View::make('/staticpage/introduce', compact('data'));
	}
	public function manufacture() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/co-so-san-xuat',
			'en' => '/manufacture?lang=en'
		);
		$data = StaticPage::getByID(2);
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Manufacture";
        $this->layout->content = View::make('/staticpage/manufacture', compact('data'));
	}
	public function mark() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/dau-an',
			'en' => '/mark?lang=en'
		);
		$data = StaticPage::getByID(3);
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Mark";
        $this->layout->content = View::make('/staticpage/mark', compact('data'));
	}
	public function policy() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/chinh-sach',
			'en' => '/policy?lang=en'
		);
		$data = StaticPage::getByID(4);
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Policy";
        $this->layout->content = View::make('/staticpage/policy', compact('data'));
	}
	public function contact() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/lien-he',
			'en' => '/contact?lang=en'
		);
		
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
		$this->layout->title = "Contact";
        $this->layout->content = View::make('/staticpage/contact');
	}
}
