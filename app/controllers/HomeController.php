<?php
class HomeController extends BaseController {
	protected $layout = 'layouts.default';
	public function index() {
		if(!empty($_GET['lang']) && $_GET['lang'] == 'en') {
			$lang = 'en';
		}else {
			$lang = 'vn';
		}		
		App::setLocale($lang);
		/*Set lang url*/
    	$lang_url = array(
			'vn' => '/trang-chu',
			'en' => '/home?lang=en'
		);
		/*Get highlight product*/
		$products_highlight = Product::getProductsHighlight(5,0);
		$arr_products_highlight = array();
		if(!empty($products_highlight)) {
			foreach ($products_highlight as $key => $product) {
				$arr_products_highlight[$key]['product'] = $product;
				/*Get category*/
				$arr_products_highlight[$key]['category'] = Category::getCategoryByID($product->category_id);
			}
		}
		/*Get new product*/
		$products_new = Product::getProductsNew(3,0);
		$arr_products_new = array();
		if(!empty($products_new)) {
			foreach ($products_new as $key => $product) {
				$arr_products_new[$key]['product'] = $product;
				/*Get category*/
				$arr_products_new[$key]['category'] = Category::getCategoryByID($product->category_id);
			}
		}
		//dump($arr_products_new);
		/*Get introduce*/
		$introduce = StaticPage::getStaticPageByAlias('gioi-thieu');
		$arr_products = array();
		/*Get categories have parrent is product*/
		$categories = Category::getCategoriesActiveByParentID(1);
		if(!empty($categories)) {
			foreach ($categories as $key => $category) {
				$arr_products[$key]['category'] = $category;
				/*Get Products by category*/
				$arr_products[$key]['products'] = \Product::getProductsByCategoryID($category->id, 4, 0);
			}
		}
		$limit = 2;
		$offset = 0;
		/*Get news articles*/
		$news_articles = Article::getArticlesByCategoryID(2, $limit, $offset);
		/*Get culinary articles*/
		$culinary_articles = Article::getArticlesByCategoryID(3, 5, $offset);

		$this->layout->title = "Dashboard";
		view::share('arr_products_highlight', $arr_products_highlight);
		view::share('lang', $lang);
		view::share('lang_url', $lang_url);
        $this->layout->content = View::make('home',compact('arr_products_new', 'introduce' ,'arr_products','news_articles','culinary_articles','lang'));
	}
}
