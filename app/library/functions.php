<?php
/*This is function create alias of title*/
function alias($model, $title) {
	$alias = Str::slug($title);
    $aliasCount = count( $model->whereRaw("title REGEXP '^{$alias}(-[0-9]*)?$'")->get() );
    return ($aliasCount > 0) ? "{$alias}-{$aliasCount}" : $alias;
}

function dump($value) {
	echo '<pre>'; print_r($value); echo '<pre>'; exit;

}

function resizeImage($image_root_path, $thumb_type = 'A', $path = 'uploads/image/') {
    switch ($thumb_type) {
		case 'A':			
			$width = '120';
			$height = '120';
			break;
		case 'B':
			$width = '400';
			$height = '150';
			break;
		case 'C':
			$width = '400';
			$height = '300';
			break;
		case 'D':
			$width = '1400';
			$height = '600';
			break;
	}
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
    /*Get file name of image root*/
    $path_parts = pathinfo($image_root_path);        
    $file_name = $path_parts['filename'];
    $ext = $path_parts['extension'];

    $path = $path.$file_name.'-'.$width.'x'.$height.'.'.$ext;
    if(Image::make($image_root_path)->resize($width, $height)->save($path))
        return true;
    return false;
}

function getThumbImage($image, $thumb_type = 'A', $path = 'uploads/image/') {
   switch ($thumb_type) {
		case 'A':
			$type = '-120x120';
			break;
		case 'B':
			$type = '-400x150';
			break;
		case 'C':
			$type = '-400x300';
			break;
		case 'D':
			$type = '-1400x600';
			break;
	}
    
    /*Get file name of image root*/
    $path_parts = pathinfo($image);        
    $file_name = $path_parts['filename'];
    $ext = $path_parts['extension'];

    $path = $path.$file_name.$type.'.'.$ext;
    
    return $path;
}
?>